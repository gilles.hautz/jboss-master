FROM jboss/wildfly
COPY --chown=jboss host-master.xml /opt/jboss/wildfly/domain/configuration
COPY --chown=jboss host-slave.xml /opt/jboss/wildfly/domain/configuration
COPY --chown=jboss mgmt-users.properties /opt/jboss/wildfly/domain/configuration
COPY --chown=jboss mgmt-groups.properties /opt/jboss/wildfly/domain/configuration
CMD [ "/opt/jboss/wildfly/bin/domain.sh", "--host-config=host-master.xml", "-b", "0.0.0.0", "-bmanagement", "0.0.0.0"]

