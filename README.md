# jboss-master

Test d'integration continue.
Ce projet n'a aucun intérêt et ne sert qu'a tester l'environnement CI/CD de GitLab.

Start master :
sudo docker run -p 9999:9999 -p 9990:9990 -it registry.gitlab.com/gilles.hautz/jboss-master /opt/jboss/wildfly/bin/domain.sh --host-config=host-master.xml -bmanagement 0.0.0.0

Start slave :
sudo docker run -it registry.gitlab.com/gilles.hautz/jboss-master /opt/jboss/wildfly/bin/domain.sh --host-config=host-slave.xml --master-address=your.ip.address

Publication de l'application :
sh deploy.sh
